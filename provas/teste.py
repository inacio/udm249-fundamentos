#!/usr/bin/python

from subprocess import call
import sys
import os

argc = len(sys.argv)
if argc != 3:
  print "Numero de argumentos invalidos"
  print
  print "Uso:"
  print sys.argv[0]+" <csv file> <tex file>"
  print
  sys.exit(1)

fp = open(sys.argv[1])
linhas=fp.readlines()
call (["pdflatex",sys.argv[2]])
call (["pdflatex",sys.argv[2]])
main = sys.argv[2].split(".")
main = main[0]+".pdf"
files=[main]
for i in range(1,len(linhas)):
  linha=linhas[i].split(":")
  fout = linha[0]+".tex"
  call (["cp",sys.argv[2],fout])
  dados = "Nome: "+linha[1][:-1]+" -  Matricula: "+linha[0]
  sed = "s/Nome: Matricula:/"+dados+"/g"
  call (["sed", "-i", sed, fout])
  call (["pdflatex",fout])
  call (["pdflatex",fout])
  fName = linha[0]+".pdf"
  files += [fName]

fOut = sys.argv[2].split(".")
fOut = "-sOutputFile="+fOut[0]+"-p.pdf"
gs = ["gs","-q","-dNOPAUSE","-dBATCH","-sDEVICE=pdfwrite",fOut,"-f"]+files
call (gs)

files = files[1:]
for i in files:
  fileDel = i.split(".")
  fileDel = fileDel[0]+".*"
  print fileDel
  os.system("rm -rf "+fileDel)

