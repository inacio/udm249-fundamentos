#!/bin/bash
repo=udm249-fundamentos
IFrnp=200.129.2.59
IFlocal=10.50.30.62
IFtelematica=192.168.10.254

if [ $# -lt 1 ]
then
  echo "Nao foi fornecido parametro"
  echo "Uso:"
  echo "$0 opcao"
  echo "  rnp   = link externo da RNP"
  echo "  local = link da rede local IFCE"
  echo "  tele  = link da rede telematica"
  exit 1
fi

case $1 in
"rnp")
  git remote set-url origin git@$IFrnp:/$repo.git
;;
"local")
  git remote set-url origin git@$IFlocal:/$repo.git
;;
"tele")
  git remote set-url origin git@$IFtelematica:/$repo.git
;;
esac
